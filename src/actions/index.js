export const ADD_CLIP = "ADD_CLIP";
export const DELETE_CLIP = "DELETE_CLIP";
export const EDIT_CLIP = "EDIT_CLIP";
export const SELECT_CLIP = "SELECT_CLIP";
export const RESET_ALL = "RESET_ALL";
export const LOAD_FROM_MEMORY = "LOAD_FROM_MEMORY";
export const TAGS_SEARCH = "TAGS_SEARCH";

export const addClip = d => {
  return {
    type: "ADD_CLIP",
    payload: d
  };
};

export const editClip = d => {
  return {
    type: "EDIT_CLIP",
    payload: d
  };
};

export const selectClip = d => {
  return {
    type: "SELECT_CLIP",
    payload: d
  };
};

export const deleteClip = d => {
  return {
    type: "DELETE_CLIP",
    payload: d
  };
};

export const resetAll = () => {
  return {
    type: "RESET_ALL"
  };
};

export const loadFromMemory = () => {
  return {
    type: "LOAD_FROM_MEMORY"
  };
};

export const tagsSearch = d => {
  return {
    type: "TAGS_SEARCH",
    payload: d
  };
};
