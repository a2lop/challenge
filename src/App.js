import React, { Component } from "react";

import { connect } from "react-redux";

import {
  addClip,
  editClip,
  selectClip,
  deleteClip,
  resetAll,
  loadFromMemory,
  tagsSearch
} from "./actions/index";

import "./css/video-react.css";
import "./css/bootstrap/dist/css/bootstrap.min.css";

import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faTimes } from "@fortawesome/free-solid-svg-icons";

import loading from "./img/loading.svg";

import "./App.css";

import { Player, ControlBar } from "video-react";

import Clip from "./components/Clip";

import {
  Container,
  Row,
  Col,
  Form,
  Input,
  Label,
  Button,
  FormGroup
} from "reactstrap";

import ScrollMenu from "react-horizontal-scrolling-menu";

library.add(faEdit, faTimes);

function timeFormat(t) {
  try {
    t = parseInt(t);
    var m = parseInt(t / 60);
    var s = t - m * 60;
    return (m > 9 ? m : "0" + m) + ":" + (s > 9 ? s : "0" + s);
  } catch (error) {}
  return "00:00";
}

function timeToSecs(t) {
  try {
    var d = t.split(":");
    return parseInt(d[0]) * 60 + parseInt(d[1]);
  } catch (error) {}
  return 0;
}

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      txtUrl: "https://media.w3.org/2010/05/sintel/trailer_hd.mp4",
      txtClipName: "",
      txtClipStart: "",
      txtClipEnd: "",
      txtTags: "",
      txtSearch: "",
      mainUrl: "",
      placeholderEndTime: "",
      selectedClip: {},
      isEditing: false,
      // clips: [],
      selectedStartTime: 0,
      selectedEndTime: 0,
      isFirstLoad: true,
      currentClip: 0,
      duration: 0,
      showLoading: false,
      isVideoLoaded: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.loadMainVideo = this.loadMainVideo.bind(this);
    this.handleNewClip = this.handleNewClip.bind(this);

    this.hotkeyHandler = this.hotkeyHandler.bind(this);

    //video events

    this.onVideoLoad = this.onVideoLoad.bind(this);
  }

  componentDidMount() {
    this.setState({ mainUrl: localStorage.videoUrl || "" });

    document.addEventListener("keyup", this.hotkeyHandler, false);
  }

  hotkeyHandler(e) {
    if (e.keyCode === 37) {
      this.setState(
        {
          currentClip:
            this.state.currentClip - 1 >= 0 ? this.state.currentClip - 1 : 0
        },
        function() {
          this.changeVideo(this.props.clips[this.state.currentClip]);
        }
      );
    } else if (e.keyCode === 39) {
      this.changeVideo(
        this.props.clips[
          this.state.currentClip + 1 === this.props.clips.length
            ? this.state.currentClip
            : this.state.currentClip + 1
        ]
      );
    }
  }

  handleStateChange(state, prevState) {
    this.setState({
      player: state,
      currentTime: state.currentTime,
      showLoading: this.state.duration - state.currentTime < 3
    });

    if (state.currentTime > this.state.duration) {
      this.onVideoEnd();
    }
  }

  handleChange(e) {
    this.setState({ txtUrl: e.target.value });
  }

  loadMainVideo(e) {
    e.preventDefault();
    this.setState({ mainUrl: this.state.txtUrl });
    localStorage.setItem("videoUrl", this.state.txtUrl);
  }

  editClip(c) {
    this.setState({
      selectedClip: c,
      txtClipName: c.name,
      txtClipStart: c.start,
      txtClipEnd: c.end,
      txtTags: c.tags,
      isEditing: true
    });
  }

  saveClip() {
    var s = this.state.selectedClip;
    s.name = this.state.txtClipName;
    s.start = this.state.txtClipStart;
    s.end = this.state.txtClipEnd;
    s.tags = this.state.txtTags;

    this.props.editClip(s);

    this.setState({
      txtClipName: "",
      txtClipStart: "",
      txtClipEnd: "",
      txtTags: "",
      isEditing: false,
      selectedClip: {}
    });
  }

  deleteClip(c) {
    this.props.deleteClip(c.id);
  }

  changeVideo(c) {
    var clips = this.props.clips;
    var currentClip = 0;
    this.props.selectClip(c.id);
    clips.forEach(e => {
      // e.isSelected = c.id === e.id;
      if (c.id === e.id) {
        this.setState({ currentClip });
      }
      currentClip++;
    });

    var player = this.refs.player;
    var startTime = timeToSecs(c.start);
    var thisClipDuration = timeToSecs(c.end);
    this.setState({
      selectedStartTime: startTime,
      duration:
        thisClipDuration > startTime &&
        thisClipDuration < this.state.videoLength
          ? thisClipDuration
          : this.state.videoLength
    });
    player.load();
  }

  handleNewClip(e) {
    e.preventDefault();
    var timeRegex = new RegExp("^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$");
    var id = 1;
    try {
      id = this.props.clips[this.props.clips.length - 1].id + 1;
    } catch (error) {}

    var st = "00:00";
    if (timeRegex.test(this.state.txtClipStart)) {
      st = this.state.txtClipStart;
    }

    var et = this.state.placeholderEndTime;
    if (timeRegex.test(this.state.txtClipEnd)) {
      et = this.state.txtClipEnd;
    }

    this.props.addClip({
      id,
      isMain: false,
      name:
        this.state.txtClipName !== ""
          ? this.state.txtClipName
          : "nuevo clip " + id,
      start: st,
      end: et,
      tags: this.state.txtTags
    });

    this.setState({
      txtClipName: "",
      txtClipStart: "",
      txtClipEnd: "",
      txtTags: ""
    });
  }

  onChangeTxtClipStart(e) {
    e.preventDefault();
    this.setState({ txtClipStart: e.target.value });
  }

  onChangeTxtClipEnd(e) {
    e.preventDefault();
    this.setState({ txtClipEnd: e.target.value });
  }

  onChangeTxtClipName(e) {
    e.preventDefault();
    this.setState({ txtClipName: e.target.value });
  }

  onChangeTxtTags(e) {
    e.preventDefault();
    this.setState({ txtTags: e.target.value });
  }

  onChangeTxtSearch(e) {
    e.preventDefault();
    this.setState({ txtSearch: e.target.value });

    clearTimeout(this.to);
    this.to = setTimeout(() => {
      this.props.tagsSearch(this.state.txtSearch);
    }, 1000);
  }

  onVideoLoad(e) {
    if (this.state.isFirstLoad) {
      this.refs.player.subscribeToStateChange(
        this.handleStateChange.bind(this)
      );
      var player = this.refs.player.getState();

      var endTime = timeFormat(player.player.duration);
      if (localStorage.clips) {
        this.props.loadFromMemory();
      } else {
        this.props.addClip({
          id: 0,
          isMain: true,
          name: "Principal",
          start: "0:00",
          end: endTime,
          isSelected: true
        });
      }
      this.setState({
        placeholderEndTime: endTime,
        isFirstLoad: false,
        duration: player.player.duration,
        videoLength: player.player.duration,
        isVideoLoaded: true
      });
    }
  }

  onVideoEnd() {
    if (this.state.currentClip < this.props.clips.length - 1) {
      this.changeVideo(this.props.clips[this.state.currentClip + 1]);
      this.setState({ currentClip: this.state.currentClip + 1 });
    }
  }

  onVideoError() {
    alert("Hubo un error al cargar el video, intenta nuevamente");
  }

  reset(e) {
    e.preventDefault();
    this.props.resetAll();
    this.setState({
      txtUrl: "",
      txtClipName: "",
      txtClipStart: "",
      txtClipEnd: "",
      mainUrl: "",
      placeholderEndTime: "",
      selectedClip: {},
      isEditing: false,
      selectedStartTime: 0,
      selectedEndTime: 0,
      isFirstLoad: true,
      currentClip: 0,
      duration: 0,
      showLoading: false,
      isVideoLoaded: false
    });
  }

  render() {
    return (
      <Container>
        <div className="App">
          <Form onSubmit={this.loadMainVideo}>
            <Label htmlFor="new-todo">URL Video:</Label>
            <br />
            <Row>
              <Col xs="9">
                <Input
                  id="new-todo"
                  onChange={this.handleChange}
                  value={this.state.txtUrl}
                  placeholder={"URL del video mp4"}
                />
              </Col>
              <Col xs="3">
                {this.state.mainUrl === "" ? (
                  <Button>Cargar</Button>
                ) : (
                  <Button onClick={e => this.reset(e)}>Reiniciar</Button>
                )}
              </Col>
            </Row>
          </Form>
          <Row className={"player-section"}>
            <Col xs="8">
              {this.state.mainUrl !== "" ? (
                <div className={"player-container"}>
                  <Player
                    ref="player"
                    onLoadedData={this.onVideoLoad.bind(this)}
                    onEnded={this.onVideoEnd.bind(this)}
                    onError={this.onVideoError.bind(this)}
                    startTime={this.state.selectedStartTime}
                    autoPlay={true}
                  >
                    <source src={this.state.mainUrl} />
                    <ControlBar autoHide={false} />
                  </Player>
                  {this.state.showLoading ? (
                    <div className={"loading-next-video"}>
                      <img src={loading} alt={""} />
                    </div>
                  ) : null}
                </div>
              ) : null}
            </Col>
            {this.state.isVideoLoaded ? (
              <Col xs="4">
                <Form onSubmit={this.handleNewClip} className={"new-clip-form"}>
                  <Label>Nuevo clip</Label>
                  <FormGroup>
                    <Label for="txtClipName">Nombre</Label>
                    <Input
                      type="text"
                      name="txtClipName"
                      value={this.state.txtClipName}
                      onChange={this.onChangeTxtClipName.bind(this)}
                      placeholder="Nombre de video"
                    />
                  </FormGroup>
                  <Row>
                    <Col xs="6">
                      <FormGroup>
                        <Label for="txtStart">Inicio</Label>
                        <Input
                          type="text"
                          name="txtStart"
                          value={this.state.txtClipStart}
                          onChange={this.onChangeTxtClipStart.bind(this)}
                          placeholder="00:00"
                        />
                      </FormGroup>
                    </Col>
                    <Col xs="6">
                      <FormGroup>
                        <Label for="txtStart">Fin</Label>
                        <Input
                          type="text"
                          name="txtStart"
                          value={this.state.txtClipEnd}
                          onChange={this.onChangeTxtClipEnd.bind(this)}
                          placeholder={this.state.placeholderEndTime}
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <FormGroup>
                    <Label for="txtTags">Etiquetas</Label>
                    <Input
                      type="text"
                      name="txtTags"
                      value={this.state.txtTags}
                      onChange={this.onChangeTxtTags.bind(this)}
                      placeholder="etiqueta1,etiqueta2,..."
                    />
                  </FormGroup>
                  {!this.state.isEditing ? (
                    <Button>Añadir</Button>
                  ) : (
                    <Row>
                      <Col xs="6">
                        <Button onClick={e => this.saveClip()}>Guardar</Button>
                      </Col>
                      <Col xs="6">
                        <Button
                          onClick={e => {
                            this.setState({
                              selectedClip: {},
                              txtClipName: "",
                              txtClipStart: "",
                              txtClipEnd: "",
                              isEditing: false
                            });
                          }}
                        >
                          Cancelar
                        </Button>
                      </Col>
                    </Row>
                  )}
                </Form>
              </Col>
            ) : null}
          </Row>
        </div>
        {this.state.isVideoLoaded ? (
          <div className={"list-clips"}>
            <div>
              {/* <Row className={"search-section"}>
                <Col xs="2"> */}
              <Label>Buscar</Label>
              {/* </Col>
                <Col xs="10"> */}
              <Input
                type="text"
                name="txtSearch"
                value={this.state.txtSearch}
                onChange={this.onChangeTxtSearch.bind(this)}
                placeholder="Busqueda por etiquetas"
              />
              {/* </Col>
              </Row> */}
            </div>
            <ScrollMenu
              data={this.props.clips.map((el, i) => {
                return (
                  <div className={"clip-thumbnail"} key={i}>
                    {!el.isMain ? (
                      <Button
                        className={"btn-clip-edit"}
                        onClick={e => this.editClip(el)}
                      >
                        <FontAwesomeIcon icon="edit" />
                      </Button>
                    ) : null}
                    {!el.isMain ? (
                      <Button
                        className={"btn-clip-delete"}
                        onClick={e => this.deleteClip(el)}
                      >
                        <FontAwesomeIcon icon="times" />
                      </Button>
                    ) : null}
                    {/* <Button onClick={e => this.editClip(el)}> */}
                    <div onClick={e => this.changeVideo(el)}>
                      <Clip info={el} />
                    </div>
                    {/* </Button> */}
                  </div>
                );
              })}
            />
          </div>
        ) : null}
      </Container>
    );
  }
}

// export default App;

const mapStateToProps = state => {
  return {
    clips: state.clips
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addClip: d => dispatch(addClip(d)),
    editClip: d => dispatch(editClip(d)),
    selectClip: d => dispatch(selectClip(d)),
    deleteClip: d => dispatch(deleteClip(d)),
    resetAll: () => dispatch(resetAll()),
    loadFromMemory: () => dispatch(loadFromMemory()),
    tagsSearch: d => dispatch(tagsSearch(d))
  };
};

const AppView = connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
export default AppView;
