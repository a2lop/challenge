import {
  ADD_CLIP,
  EDIT_CLIP,
  SELECT_CLIP,
  DELETE_CLIP,
  RESET_ALL,
  LOAD_FROM_MEMORY,
  TAGS_SEARCH
} from "../actions/index";
const initialState = {
  clips: [],
  allClips: []
};
const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_CLIP:
      state.clips.push(action.payload);

      localStorage.setItem("clips", JSON.stringify(state.clips));

      return state;

    case EDIT_CLIP:
      var clips = state.clips;
      clips.forEach(e => {
        if (e.id === action.payload.id) {
          e = action.payload;
        }
      });
      state = Object.assign({}, state, {
        clips,
        allClips: clips
      });
      localStorage.setItem("clips", JSON.stringify(clips));
      return state;
    case SELECT_CLIP:
      clips = state.clips;
      clips.forEach(e => {
        e.isSelected = e.id === action.payload;
      });
      state = Object.assign({}, state, {
        clips,
        allClips: clips
      });
      localStorage.setItem("clips", JSON.stringify(clips));
      return state;
    case DELETE_CLIP:
      var remainClips = [];
      state.clips.forEach(e => {
        if (e.id !== action.payload) {
          remainClips.push(e);
        }
      });
      state = Object.assign({}, state, {
        clips: remainClips,
        allClips: remainClips
      });
      localStorage.setItem("clips", JSON.stringify(remainClips));
      return state;

    case LOAD_FROM_MEMORY:
      try {
        clips = JSON.parse(localStorage.clips);
        clips.forEach(e => {
          e.isSelected = e.id === 0;
        });
        state = Object.assign({}, state, {
          clips: clips,
          allClips: clips
        });
      } catch (error) {}

      return state;

    case RESET_ALL:
      state = Object.assign({}, state, {
        clips: [],
        allClips: []
      });
      localStorage.removeItem("clips");
      localStorage.removeItem("videoUrl");
      return state;
    case TAGS_SEARCH:
      if (!action.payload || action.payload === "") {
        clips = state.allClips;
      } else {
        clips = state.allClips.filter(
          e => e.tags && e.tags.indexOf(action.payload) >= 0
        );
      }
      state = Object.assign({}, state, {
        clips
      });
      return state;
    default:
      return state;
  }
};
export default rootReducer;
