import React from "react";
import "../App.css";
import iconPlay from "../img/icon_play.svg";

import { Label } from "reactstrap";

export default class Clip extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div
        className={
          "clipContent" + (this.props.info.isSelected ? " clipSelected" : "")
        }
      >
        <img src={iconPlay} alt={""} />
        <Label className="clipTitle">{this.props.info.name}</Label>
        <Label className="clipSubtitle">
          {this.props.info.start} - {this.props.info.end}
        </Label>
        <Label className="clipTags">
          {this.props.info.tags}
        </Label>
      </div>
    );
  }
}
